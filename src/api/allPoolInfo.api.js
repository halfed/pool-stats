import httpClient from './httpClient';

const END_POINT = '/getAllPoolInfo';


const getAllPoolInfo = () => httpClient.get(END_POINT);

export {
  getAllPoolInfo
}
