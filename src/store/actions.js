import { getAllPoolInfo } from '@/api/allPoolInfo.api.js';

export const getAllPoolStatsInfo = ({ commit }) => {

  return getAllPoolInfo()
    .then(poolData => {
      commit('populatePoolInfo', poolData.data);
    });
}
