<?php
$getAllPlayers = 'SELECT player_id, first_name, wins, loss FROM players';

$getPlayerInfo = 'SELECT wins, loss FROM players WHERE player_id = :playerId';

$getMaxPlayerId = 'SELECT max(player_id) FROM players';

$getMaxDailyId = 'SELECT max(daily_id) FROM daily_wins';

$insertPlayer = 'INSERT INTO players (first_name, wins, loss) VALUES (:fname, :win, :loss)';

$updatePlayer = 'UPDATE players SET wins = :win, loss = :loss  WHERE player_id = :id';

$insertDailyWins = 'INSERT INTO daily_wins (win, loss, player_id, date_played) VALUES (:win, :loss, :playerId, :date)';

$insertPlayerToDaily = 'INSERT INTO player_to_daily_wins (player_id, daily_id) VALUES (:playerId, :dailyId)';

class PreparedStatements {
	public $getAllMatches = '
		SELECT gm.match_id, gm.opponent1_id, gm.opponent2_id, gm.opponent1_score, gm.opponent2_score,
		(SELECT p.first_name 
		FROM `players` as p
		WHERE p.player_id = gm.opponent1_id) as opponent1_name,
		(SELECT p.first_name 
		FROM `players` as p
		WHERE p.player_id = gm.opponent2_id) as opponent2_name
		FROM `game_match` as gm
	';

	public $getAllPlayers = 'SELECT player_id, first_name, wins, loss FROM players';

	public $getPlayerInfo = 'SELECT wins, loss FROM players WHERE player_id = :playerId';

	public $getMaxPlayerId = 'SELECT max(player_id) FROM players';

	public $getMaxDailyId = 'SELECT max(daily_id) FROM daily_wins';

	public $insertPlayer = 'INSERT INTO players (first_name, wins, loss) VALUES (:fname, :win, :loss)';

	public $updatePlayer = 'UPDATE players SET wins = :win, loss = :loss  WHERE player_id = :id';

	public $insertDailyWins = 'INSERT INTO daily_wins (win, loss, player_id, date_played) VALUES (:win, :loss, :playerId, :date)';

	public $insertPlayerToDaily = 'INSERT INTO player_to_daily_wins (player_id, daily_id) VALUES (:playerId, :dailyId)';
}

/*
SELECT gm.match_id, gm.opponent1_id, gm.opponent2_id, gm.opponent1_score, gm.opponent2_score,
(SELECT p.first_name 
FROM `players` as p
WHERE p.player_id = gm.opponent1_id) as opponent1_name,
(SELECT p.first_name 
FROM `players` as p
WHERE p.player_id = gm.opponent2_id) as opponent2_name
FROM `game_match` as gm
*/
?>