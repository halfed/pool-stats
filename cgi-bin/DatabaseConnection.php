<?php
	require_once("DBSettings.php");

	class Database {
		public $connection;
		public $connectionMessage;

		function __construct() {
			$this->open_db_connection();
		}

		public function open_db_connection() {
			$this->connection = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME, DB_USER, DB_PASS);
			// set the PDO error mode to exception
			$this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}

		public function query($sql) {
			$statement = $this->connection->prepare($sql);			
			return $statement;
		}
	}

	$database = new Database();
?>